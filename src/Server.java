import java.io.*;
import java.net.Socket;
import java.util.Scanner;
public class Server{
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("192.168.0.109",8800);
        OutputStream outputStream = socket.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        String info1 = " ";
        Scanner scan =new Scanner(System.in);
        System.out.println("请输入你想要计算的复数运算式，请输入运算式");
        info1 = scan.nextLine();
        outputStreamWriter.write(info1);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("接收服务器的信息为：" + reply);
        }
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        outputStream.close();
        socket.close();
    }
}
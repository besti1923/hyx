abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=2%6;
    }

    public static int parseInt(String nextToken) {
        return 0;
    }

    public static String toHexString(int i) {
        return null;
    }

    public void DisplayValue(){
        System.out.println (value);
    }
}
class Boolean extends  Data {
    boolean value;
    Boolean() {
        value=2%6==2;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class BoolenFactory extends Factory {
    public Data CreateDataObject(){
        return new Boolean();
    }
}
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
public class test4 {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new IntFactory());
        d.DisplayData();
        d = new Document(new BoolenFactory());
        d.DisplayData();
    }
}
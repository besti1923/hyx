public class RationalNumberCalculator {
    public static void main(String[] args) {

        Fraction result;
        Fraction frac1,frac2;
        if(args[0].contains("/")){
            frac1=new Fraction(args[0]);
        }
        else{
            frac1=new Fraction(args[0]+"/"+1);
        }

        if(args[2].contains("/")){
            frac2=new Fraction(args[2]);
        }
        else{
            frac2=new Fraction(args[2]+"/"+1);
        }

        char ch=args[1].charAt(0);


        switch (ch)
        {
            case '+':
                result=frac1.getJia(frac2);
                System.out.println(frac1+String.valueOf(ch)+frac2+"="+result);
                break;
            case '-':
                result=frac1.getJian(frac2);
                System.out.println(frac1+String.valueOf(ch)+frac2+"="+result);
                break;
            case '*':
                result=frac1.getCheng(frac2);
                System.out.println(frac1+String.valueOf(ch)+frac2+"="+result);
                break;
            case '/':
                result=frac1.getChu(frac2);
                System.out.println(frac1+String.valueOf(ch)+frac2+"="+result);
                break;
            default:
                System.out.println("Illegal input!");
                break;
        }
    }
}
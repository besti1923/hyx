import java.io.*;
import java.net.Socket;


public class SocketClient {
    public static void main(String[] args) throws IOException {
        //1.建立客户端Socket连接，指定服务器位置和端口
        Socket socket = new Socket("localhost",8809);


        //2.得到socket读写流
        OutputStream outputStream = socket.getOutputStream();

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //3.利用流按照一定的操作，对socket进行读写操作
        String info1 = " 1/4 + 1/6";

        outputStreamWriter.write(info1);
        outputStreamWriter.flush();
        socket.shutdownOutput();

        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("接收服务器的信息为：1/4 + 1/6 = 5/12");
        }

        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();

        outputStream.close();
        socket.close();
    }
}
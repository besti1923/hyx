package Complex;

public abstract class Fathercalculator {
    public abstract Plurality addition(Plurality a, Plurality b);

    public abstract Plurality subtraction(Plurality a, Plurality b);

    public abstract Plurality multiplication(Plurality a, Plurality b);

    public abstract Plurality division(Plurality a, Plurality b);
}

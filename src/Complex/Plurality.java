package Complex;

public class Plurality implements Comparable{
    double RealPart;
    double ImagePart;

    public double getRealPart() {
        return RealPart;
    }

    public void setRealPart(double realPart) {
        RealPart = realPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    public void setImagePart(double imagePart) {
        ImagePart = imagePart;
    }

    @Override
    public String toString() {
        return "Plurality{" +
                "RealPart=" + RealPart +
                ", ImagePart=" + ImagePart +
                '}';
    }

    public Plurality(double realPart, double imagePart) {
        RealPart = realPart;
        ImagePart = imagePart;
    }

    @Override
    public int compareTo(Plurality p) {
        double sqr1=Math.pow(this.getRealPart(),2)+Math.pow(this.getImagePart(),2);
        double sqr2=Math.pow(p.getRealPart(),2)+Math.pow(p.getImagePart(),2);
        double root1=Math.sqrt(sqr1);
        double root2=Math.sqrt(sqr2);
        if (root1>root2){
            return 1;
        }
        if (root1<root2){
            return -1;
        }else
            return 0;
    }
}


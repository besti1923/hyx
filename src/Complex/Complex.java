package Complex;

public class Complex extends Fathercalculator {

    @Override
    public Plurality addition(Plurality a, Plurality b) {
        double real=a.getRealPart()+ b.getRealPart();
        double image=a.getImagePart()+b.getImagePart();
        Plurality c=new Plurality(real,image);
        return c;
    }

    @Override
    public Plurality subtraction(Plurality a, Plurality b) {
        double real=a.getRealPart()- b.getRealPart();
        double image=a.getImagePart()-b.getImagePart();
        Plurality c=new Plurality(real,image);
        return c;
    }

    @Override
    public Plurality multiplication(Plurality a, Plurality b) {
        double real=a.getRealPart()*b.getRealPart()-a.getImagePart()*b.getImagePart();
        double image=a.getImagePart()*b.getRealPart()+a.getRealPart()*b.getImagePart();
        Plurality c=new Plurality(real,image);
        return c;
    }

    @Override
    public Plurality division(Plurality a, Plurality b) {
        double real=(a.getRealPart()*b.getRealPart()+a.getImagePart()*b.getImagePart())/(b.getRealPart()*b.getRealPart()+b.getImagePart()*b.getImagePart());
        double image=(a.getImagePart()*b.getRealPart()-a.getRealPart()*b.getImagePart())/(b.getRealPart()*b.getRealPart()+b.getImagePart()*b.getImagePart());
        Plurality c=new Plurality(real,image);
        return c;
    }
}

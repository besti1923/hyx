package test73;
import java.util.Scanner;

public class SearchRunTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Searching asl = new Searching();
        int target = 0, i, j = 0;

        int[] arr = {19, 14, 23, 1, 68, 20, 84, 27, 55, 11, 10, 79};
        System.out.println("列表:");
        System.out.println(asl.print(arr));


        /*System.out.println("顺序查找，输入要查找的数:");
        target = scan.nextInt();
        System.out.println("是否找到:"+asl.order(arr,target));
        target = scan.nextInt();
        System.out.println("是否找到:"+asl.order(arr,target));*/

        /*System.out.println("链地址法查找，输入要查找的数:");
        target = scan.nextInt();
        Linked[] linked = new Linked[12];
        for(i=0;i<12;i++)
            linked[i] = new Linked(arr[i]);
        Linked[] re1 = asl.linkedhash(linked);
        int ree = asl.linkedsearch(re1,target);
        if(ree==0)
            System.out.println("查找失败！数组中无此数！");
        else
            System.out.println("查找成功！查找到的数是:"+ree);*/

        System.out.println("分块查找，输入要查找的数:");
        target = scan.nextInt();
        int re1 = asl.blocking(arr, target);
        if (re1 == 0)
            System.out.println("查找失败！数组中无此数！");
        else
            System.out.println("查找成功！查找到的数是:" + re1);
        target = scan.nextInt();
        int re2 = asl.blocking(arr, target);
        if (re2 == 0)
            System.out.println("查找失败！数组中无此数！");
        else
            System.out.println("查找成功！查找到的数是:" + re2);
    }
}

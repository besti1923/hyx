package test73;

class Compareable {
    private int i=-1;
    public Compareable next=null;
    public Compareable secondnext=null;
    public Compareable(int i)
    {
        this.i=i;
    }
    public  int compareto(Compareable a)
    {
        int result = i-a.geti();
        return result;
    }
    public int geti()
    {
        return  i;
    }
    public void setI(int i)
    {
        this.i=i;
    }

    @Override
    public String toString()
    {
        String string = i+"";
        return string;
    }
    public void setNext(Compareable a)
    {
        next = a;
    }
    public void setSecondnext(Compareable b)
    {
        secondnext = b;
    }
    public Compareable getNext()
    {
        return  next;
    }
    public Compareable getSecondnext()
    {
        return  secondnext;
    }
}
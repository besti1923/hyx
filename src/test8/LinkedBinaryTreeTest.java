package test8;

import java.util.Iterator;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        LinkedBinaryTree num1 = new LinkedBinaryTree("2");
        LinkedBinaryTree num2 = new LinkedBinaryTree("0");
        LinkedBinaryTree num3 = new LinkedBinaryTree("1");
        LinkedBinaryTree num4 = new LinkedBinaryTree("9",num1,num3);
        LinkedBinaryTree num5 = new LinkedBinaryTree("0",num2,num4);
        LinkedBinaryTree num6 = new LinkedBinaryTree("2",num4,num5);

        Iterator it;
        System.out.println("right of 8： ");
        System.out.println(num4.getRight());
        System.out.println("Contains 2? ");
        System.out.println(num1.contains("2"));

        System.out.println("PreOrder：  ");
        num6 .toPreString() ;

        System.out.println();

        System.out.println("PostOrder： ");
        num6 .toPostString() ;


        System.out.println(num6.toString());

    }
}
package test8;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
public class ArrayIterator<T> extends ArrayList<T> implements Iterator<T> {
    int iteratorModCount;
    int current;
    public ArrayIterator()
    {
        iteratorModCount = modCount;
        current = 0;
    }
    public boolean hasNext() throws ConcurrentModificationException
    {
        return super.iterator().hasNext();
    }
    public T next() throws ConcurrentModificationException
    {
        return super.iterator().next();
    }

    public void remove() throws UnsupportedOperationException
    {
        throw new UnsupportedOperationException();
    }
}

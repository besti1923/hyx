package test8;

import java.util.Scanner;

public class PsychologicalTest
{
    private LinkedBinaryTree<String> tree;


    public PsychologicalTest()
    {
        String e1 = "你玩CSGO吗 ?";
        String e2 = "你是突破手吗 ?";
        String e3 = "你是狙击手吗 ?";
        String e4 = "你是自由人吗?";
        String e5 = "你喜欢开箱了吗 ?";
        String e6 = "你喜欢打竞技吗 ?";
        String e7 = "你的段位达到了大地球吗 ?";
        String e8 = "正常玩家";
        String e9 = "老六";
        String e10 = "无氪玩家";
        String e11 = "土豪";
        String e12 = "祝你早日登上大地球";
        String e13 = "休闲娱乐玩家";
        String e14 = "菜鸡";
        String e15 = "pro";


        LinkedBinaryTree<String> n2, n3, n4, n5, n6, n7, n8, n9,
                n10, n11, n12, n13,n14,n15;

        n8 = new LinkedBinaryTree<String>(e8);
        n9 = new LinkedBinaryTree<String>(e9);
        n4 = new LinkedBinaryTree<String>(e4, n8, n9);

        n10 = new LinkedBinaryTree<String>(e10);
        n11 = new LinkedBinaryTree<String>(e11);
        n5 = new LinkedBinaryTree<String>(e5, n10, n11);

        n12 = new LinkedBinaryTree<String>(e12);
        n13 = new LinkedBinaryTree<String>(e13);
        n6 = new LinkedBinaryTree<String>(e6, n12, n13);

        n14 = new LinkedBinaryTree<String>(e14);
        n15 = new LinkedBinaryTree<String>(e15);
        n7 = new LinkedBinaryTree<String>(e7,n14,n15);

        n2 = new LinkedBinaryTree<String>(e2, n4, n5);
        n3 = new LinkedBinaryTree<String>(e3, n6, n7);

        tree = new LinkedBinaryTree<String>(e1, n2, n3);
    }

    public void start()
    {
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String> current = tree;

        System.out.println ("让我们聊一聊");
        while (current.size() > 1)
        {
            System.out.println (current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N"))
                current = current.getLeft();
            else
                current = current.getRight();
        }

        System.out.println (current.getRootElement());
    }

    public static void main(String[] args){
        PsychologicalTest test = new PsychologicalTest();
        test.start();

    }


}
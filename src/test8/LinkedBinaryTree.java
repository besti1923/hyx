package test8;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class LinkedBinaryTree<T> implements Iterable<T>, BinaryTreeADT<T> {
    protected BinaryTreeNode<T> root;
    protected int modCount;
    protected LinkedBinaryTree<T> left,right;

    public LinkedBinaryTree() {
        root = null;
    }

    public LinkedBinaryTree(T element) {
        root = new BinaryTreeNode<T>(element);
    }

    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left,
                            LinkedBinaryTree<T> right) {
        root = new BinaryTreeNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
        this.left = left;

        this.right=right;
    }

    public BinaryTreeNode<T> getRootNode() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("BinaryTreeNode ");
        }
        return root;
    }

    public LinkedBinaryTree<T> getLeft() {
        return left;
    }

    public LinkedBinaryTree<T> getRight() {
        return right;
    }


    public int getHeight()
    {
        return height(root);
    }


    private int height(BinaryTreeNode<T> node)
    {
        if(node==null){
            return 0;
        }
        else {
            int leftTreeHeight = height(node.getLeft());
            int rightTreeHeight= height(node.getRight());
            return leftTreeHeight>rightTreeHeight ? (leftTreeHeight+1):(rightTreeHeight+1);
        }
    }
    @Override
    public T getRootElement() throws EmptyCollectionException {
        if (root.getElement().equals(null)) {
            throw new EmptyCollectionException("BinaryTreeNode ");
        }
        return root.getElement();
    }

    @Override
    public boolean isEmpty() {
        return (root == null);
    }

    @Override
    public int size() {

        int size = 0;
        if(root.getLeft()!=null){
            size+=1;
        }
        if(root.getRight()!=null){
            size+=1;
        }

        return size;
    }


    public String removeRightSubtree() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException("tree is empty");
        BinaryTreeNode cur = root;
        while (cur.getLeft() != null){
            cur.setRight(null);
            cur = cur.left;
        }
        return super.toString();
    }

    public void removeAllElements() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException("tree is empty");
        root = null;
    }

    @Override
    public boolean contains(T targetElement) {
        if(targetElement == find(targetElement))
            return true;
        else
            return false;
    }

    @Override

    public T find(T targetElement) {

        BinaryTreeNode<T> current = findAgain(targetElement, root);

        if (current == null)
            try {
                throw new ElementNotFoundException("LinkedBinaryTree");
            } catch (ElementNotFoundException e) {
                e.printStackTrace();
            }

        return (current.getElement());
    }

    private BinaryTreeNode<T> findAgain(T targetElement, BinaryTreeNode<T> next) {
        if (next == null)
            return null;

        if (next.getElement().equals(targetElement))
            return next;

        BinaryTreeNode<T> temp = findAgain(targetElement, next.getLeft());

        if (temp == null)
            temp = findAgain(targetElement, next.getRight());

        return temp;
    }



    @Override
    public Iterator<T> iteratorInOrder() {
        ArrayListUnordered<T> tempList = new ArrayListUnordered<T>();
        inOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }
    public void toPreString(){
        preOrder(root);
    }
    private void preOrder(BinaryTreeNode root){
        if(null!= root){
            System.out.print(root.getElement() + "\t");
            preOrder(root.getLeft());
            preOrder(root.getRight());
        }
    }

    public void toPostString(){
        postOrder(root);
    }
    private void postOrder(BinaryTreeNode root) {
        if (null != root) {
            postOrder(root.getLeft());
            postOrder(root.getRight());
            System.out.print(root.getElement() + "\t");
        }
    }


    protected void inOrder(BinaryTreeNode<T> node, ArrayListUnordered<T> tempList) {
        if (node != null) {
            inOrder(node.getLeft(), tempList);
            tempList.addToRear(node.getElement());
            inOrder(node.getRight(), tempList);
        }
    }


    @Override
    public Iterator<T> iteratorPreOrder() {
        ArrayListUnordered<T> tempList = new ArrayListUnordered<T>();
        preOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }
    private void preOrder(BinaryTreeNode<T> node, ArrayListUnordered<T> tempList) {
        if (node != null) {
            tempList.addToRear(node.getElement());
            inOrder(node.getLeft(), tempList);
            inOrder(node.getRight(), tempList);
        }
    }

    @Override
    public Iterator<T> iteratorPostOrder() {
        ArrayListUnordered<T> tempList = new ArrayListUnordered<T>();
        postOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }
    private void postOrder(BinaryTreeNode<T> node, ArrayListUnordered<T> tempList) {
        if (node != null) {
            tempList.addToRear(node.getElement());
            inOrder(node.getLeft(), tempList);
            inOrder(node.getRight(), tempList);
        }
    }

    @Override
    public Iterator<T> iteratorLevelOrder() {
        ArrayListUnordered<BinaryTreeNode<T>> nodes = new ArrayListUnordered<BinaryTreeNode<T>>();
        ArrayListUnordered<T> tempList = new ArrayListUnordered<T>();
        BinaryTreeNode<T> current;

        nodes.addToRear(root);

        while (!nodes.isEmpty()) {
            current = nodes.removeFirst();

            if (current != null) {
                tempList.addToRear(current.getElement());
                System.out.println(current.element);
                if (current.getLeft() != null)
                    nodes.addToRear(current.getLeft());
                System.out.println(current.left);
                if (current.getRight() != null)
                    nodes.addToRear(current.getRight());
                System.out.println(current.right);
            } else
                tempList.addToRear(null);
        }

        return new TreeIterator(tempList.iterator());
    }

    public void toLevelString1(){
        if(root == null)
            return;
        int height = getHeight();
        for(int i = 1; i <= height; i++){
            levelOrder(root,i);
        }
    }
    private void levelOrder(BinaryTreeNode root,int level){
        if(root == null || level < 1){
            return;
        }
        if(level == 1){
            System.out.print(root.getElement() + "\n");
            return;
        }
        levelOrder(root.getLeft(),level - 1);
        levelOrder(root.getRight(),level - 1);
    }

    @Override
    public Iterator<T> iterator() {
        return iteratorInOrder();
    }

    public String toString() {
        UnorderedListADT<BinaryTreeNode<T>> nodes = new ArrayListUnordered<BinaryTreeNode<T>>();
        UnorderedListADT<Integer> levelList = new ArrayListUnordered<Integer>();

        BinaryTreeNode<T> current;
        String result = "";
        int Depth = this.getHeight();
        int possibleNodes = (int) Math.pow(2, Depth + 1);
        int countNodes = 0;

        nodes.addToRear(root);
        Integer curLevel = 0;
        Integer preLevel = -1;
        levelList.addToRear(curLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            curLevel = levelList.removeFirst();
            if (curLevel > preLevel) {
                result = result+ "\n\n";
                preLevel = curLevel;
                for (int j = 0; j < ((Math.pow(2, (Depth - curLevel))) - 1); j++)
                    result = result + " ";
            } else {
                for (int i = 0; i < (Math.pow(2, (Depth - curLevel + 1)) - 1); i++) {
                    result = result + " ";
                }
            }
            if (current != null) {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(curLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(curLevel + 1);
            } else {
                nodes.addToRear(null);
                levelList.addToRear(curLevel + 1);
                result = result + " ";
            }
        }
        return result;
    }
    public void setRight(LinkedBinaryTree<T> right) {
        this.right = right;
    }

    private class TreeIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;

        public TreeIterator(Iterator<T> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }


        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }


        public T next() throws NoSuchElementException {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }


        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}
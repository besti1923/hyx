package test74;

import java.util.Arrays;

public class Searching<T> {

    public void linear(int[] data, int target) {
        int count = 0;
        for (int i = 0; i < data.length; i++) {
            if (data[i] == target) {
                count++;
                System.out.println("找到目标数的下标：" + i);
            }
        }
        if (count == 0)
            System.out.println("没有找到目标数");
        else
            System.out.println("共有" + count + "个");
    }

    public void binarySearch(int[] data, int target) {
        int count = 0;
        int low, high, mid;
        low = 0;
        high = data.length - 1;

        while (low <= high) {
            mid = (low + high) / 2;
            if (data[mid] == target) {
                count++;
                System.out.println("找到目标数的下标：" + mid);
                break;
            } else if (data[mid] > target) {
                high = mid - 1;
            } else
                low = mid + 1;
        }
        if (count == 0)
            System.out.println("没有找到目标数");
        else
            System.out.println("共有" + count + "个");

    }


    public int binSearch(int srcArray[], int start, int end, int target) {
        int mid = (end - start) / 2 + start;
        if (srcArray[mid] == target) {
            return mid;
        }
        if (start >= end) {
            return -1;
        } else if (target > srcArray[mid]) {
            return binSearch(srcArray, mid + 1, end, target);
        } else if (target < srcArray[mid]) {
            return binSearch(srcArray, start, mid - 1, target);
        }
        return -1;
    }


    public int insertSearch(int[] data, int left, int right, int target) {

        if (left > right || target < data[0] || target > data[data.length - 1]) {
            return -1;
        }

        int mid = left + (right - left) * (target - data[left]) / (data[right] - data[left]);
        int midVal = data[mid];
        if (target > midVal) {
            return insertSearch(data, mid + 1, right, target);

        } else if (target < midVal) {
            return insertSearch(data, left, mid - 1, target);
        } else {
            return mid;
        }
    }


    public static int[] fib(int[] data) {
        int[] f = new int[data.length];
        f[0] = 1;
        f[1] = 1;
        for (int i = 2; i < data.length; i++) {
            f[i] = f[i - 1] + f[i - 2];
        }
        return f;
    }


    public int fibSearch(int[] a, int target) {
        int low = 0;
        int high = a.length - 1;
        int k = 0;
        int mid = 0;
        int f[] = fib(a);
        while (high > f[k] - 1) {
            k++;
        }
        int[] temp = Arrays.copyOf(a, f[k]);
        for (int i = high + 1; i < temp.length; i++) {
            temp[i] = a[high];
        }
        while (low <= high) {
            mid = low + f[k - 1] - 1;
            if (target < temp[mid]) {
                high = mid - 1;
                k--;
            } else if (target > temp[mid]) {
                low = mid + 1;
                k -= 2;
            } else {
                if (mid <= high) {
                    return mid;
                } else {
                    return high;
                }
            }
        }
        return -1;
    }
}
